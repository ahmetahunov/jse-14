package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IProjectRepository;
import ru.ahmetahunov.tm.api.service.IProjectService;
import ru.ahmetahunov.tm.api.service.EntityManagerService;
import ru.ahmetahunov.tm.entity.Project;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.repository.ProjectRepository;
import ru.ahmetahunov.tm.util.ComparatorUtil;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final EntityManagerService entityManagerService) {
        super(entityManagerService);
    }

    @Override
    public void persist(@Nullable final Project project) throws InterruptedOperationException {
        if (project == null) throw new InterruptedOperationException();
        if (project.getName().isEmpty()) throw new InterruptedOperationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.persist(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final Project project) throws InterruptedOperationException {
        if (project == null) throw new InterruptedOperationException();
        if (project.getName().isEmpty()) throw new InterruptedOperationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.merge(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @Nullable final Project project = repository.findOneByUserId(userId, id);
        entityManager.close();
        return project;
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @Nullable final Project project = repository.findOne(id);
        entityManager.close();
        return project;
    }

    @Override
    public @NotNull List<Project> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projects = repository.findAll();
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projects = repository.findAllByUserId(userId);
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable String comparator) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projects =
                repository.findAllWithComparator(userId, ComparatorUtil.getComparator(comparator));
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String userId, @Nullable final String projectName) {
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projects = repository.findByName(userId, "%" + projectName + "%");
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByDescription(@Nullable final String userId, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (description == null || description.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projects = repository.findByDescription(userId, "%" + description + "%");
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByNameOrDesc(@Nullable final String userId, @Nullable final String searchPhrase) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (searchPhrase == null || searchPhrase.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<Project> projects = repository.findByNameOrDesc(userId, "%" + searchPhrase + "%");
        entityManager.close();
        return projects;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.remove(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.removeById(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
