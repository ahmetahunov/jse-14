package ru.ahmetahunov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.IUserRepository;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.api.service.EntityManagerService;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.AccessForbiddenException;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import ru.ahmetahunov.tm.repository.UserRepository;
import ru.ahmetahunov.tm.util.PassUtil;
import javax.persistence.EntityManager;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final EntityManagerService entityManagerService) {
        super(entityManagerService);
    }

    @Override
    public void persist(@Nullable final User user) throws InterruptedOperationException {
        if (user == null) return;
        if (user.getLogin().isEmpty()) throw new InterruptedOperationException();
        if (contains(user.getLogin())) throw new InterruptedOperationException("Already exists.");
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final User user) {
        if (user == null) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public User findUser(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findUser(login);
        entityManager.close();
        return user;
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findOne(id);
        entityManager.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @NotNull final List<User> users = repository.findAll();
        entityManager.close();
        return users;
    }

    @Override
    public void updatePasswordAdmin(@Nullable final String userId, @Nullable final String password)
            throws InterruptedOperationException, AccessForbiddenException {
        if (userId == null || userId.isEmpty())
            throw new InterruptedOperationException("This user does not exist.");
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findOne(userId);
        if (user == null) throw new InterruptedOperationException("This user does not exist.");
        @NotNull final String hash = PassUtil.getHash(password);
        user.setPassword(hash);
        entityManager.getTransaction().begin();
        repository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void updateRole(@Nullable final String userId, @Nullable final Role role)
            throws InterruptedOperationException {
        if (userId == null || userId.isEmpty()) throw new InterruptedOperationException("This user does not exist.");
        if (role == null) throw new InterruptedOperationException("Unknown role.");
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findOne(userId);
        if (user == null) throw new InterruptedOperationException("This user does not exist.");
        user.setRole(role);
        entityManager.getTransaction().begin();
        repository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void updatePassword(
            @Nullable final String userId,
            @Nullable final String oldPassword,
            @Nullable final String password
    ) throws AccessForbiddenException, InterruptedOperationException {
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        if (oldPassword == null || oldPassword.isEmpty()) throw new AccessForbiddenException("Wrong password.");
        if (password == null || password.isEmpty()) throw new AccessForbiddenException("Wrong password.");
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findOne(userId);
        if (user == null) throw new InterruptedOperationException("This user does not exist.");
        @NotNull final String oldHash = PassUtil.getHash(oldPassword);
        if (!user.getPassword().equals(oldHash)) throw new AccessForbiddenException("Wrong password.");
        @NotNull final String hash = PassUtil.getHash(password);
        user.setPassword(hash);
        entityManager.getTransaction().begin();
        repository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void updateLogin(
            @Nullable final String userId,
            @Nullable final String login
    ) throws AccessForbiddenException, InterruptedOperationException {
        if (login == null || login.isEmpty()) throw new InterruptedOperationException("Wrong format login.");
        if (userId == null || userId.isEmpty()) throw new AccessForbiddenException("This user does not exist.");
        if (contains(login)) throw new InterruptedOperationException("Already exists.");
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findOne(userId);
        if (user == null) throw new InterruptedOperationException("This user does not exist.");
        user.setLogin(login);
        entityManager.getTransaction().begin();
        repository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable final String id) throws InterruptedOperationException {
        if (id == null || id.isEmpty()) throw new InterruptedOperationException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        entityManager.getTransaction().begin();
        repository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public boolean contains(@Nullable final String login) {
        if (login == null || login.isEmpty()) return true;
        @Nullable final User user = findUser(login);
        return user != null;
    }

}
