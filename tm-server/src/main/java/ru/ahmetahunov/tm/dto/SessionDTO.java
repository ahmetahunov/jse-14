package ru.ahmetahunov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.service.IUserService;
import ru.ahmetahunov.tm.api.service.ServiceLocator;
import ru.ahmetahunov.tm.entity.Session;
import ru.ahmetahunov.tm.enumerated.Role;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.xml.bind.annotation.XmlType;

@Getter
@Setter
@XmlType
@NoArgsConstructor
public final class SessionDTO extends AbstractEntityDTO {

	@NotNull
	private String userId;

	@Nullable
	private String signature;

	@NotNull
	private Role role;

	private long timestamp = System.currentTimeMillis();

	@NotNull
	public Session transformToSession(@NotNull final ServiceLocator serviceLocator) throws InterruptedOperationException {
		@NotNull final IUserService userService = serviceLocator.getUserService();
		@NotNull final Session session = new Session();
		session.setId(this.id);
		session.setUser(userService.findOne(this.userId));
		session.setSignature(this.signature);
		session.setTimestamp(this.timestamp);
		return session;
	}

}
