package ru.ahmetahunov.tm.repository;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.repository.ITaskRepository;
import ru.ahmetahunov.tm.entity.Task;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
public class TaskRepository implements ITaskRepository {

	@NotNull
	private final EntityManager entityManager;

	@Override
	public void persist(@NotNull final Task task) {
		entityManager.persist(task);
	}

	@Override
	public void merge(@NotNull final Task task) {
		entityManager.merge(task);
	}

	@Nullable
	@Override
	public Task findOne(@NotNull final String id) {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t WHERE t.id = :id",
				Task.class
		);
		query.setParameter("id", id);
		@NotNull final List<Task> tasks = query.getResultList();
		return (tasks.isEmpty()) ? null : tasks.get(0);
	}

	@Nullable
	@Override
	public Task findOneById(@NotNull final String userId, @NotNull final String taskId) {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t WHERE t.user.id = :userId AND t.id = :id",
				Task.class
		);
		query.setParameter("userId", userId);
		query.setParameter("id", taskId);
		@NotNull final List<Task> tasks = query.getResultList();
		return (tasks.isEmpty()) ? null : tasks.get(0);
	}

	@NotNull
	@Override
	public List<Task> findAll() {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t",
				Task.class
		);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Task> findAllByUserId(@NotNull final String userId) {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t WHERE t.user.id = :userId",
				Task.class
		);
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Task> findAllWithComparator(@NotNull final String userId, @NotNull final String comparator) {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY t." + comparator,
				Task.class
		);
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Task> findAllByProjectId(
			@NotNull final String userId,
			@NotNull final String projectId,
			@NotNull final String comparator
	) {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId ORDER BY t." + comparator,
				Task.class
		);
		query.setParameter("userId", userId);
		query.setParameter("projectId", projectId);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Task> findByName(@NotNull final String userId, @NotNull final String taskName) {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t WHERE t.user.id = :userId AND t.name LIKE :name ORDER BY t.name",
				Task.class
		);
		query.setParameter("userId", userId);
		query.setParameter("name", taskName);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Task> findByNameOrDesc(@NotNull final String userId, @NotNull final String searchPhrase) {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t WHERE t.user.id = :userId AND (t.name LIKE :phrase OR t.description LIKE :phrase)",
				Task.class
		);
		query.setParameter("userId", userId);
		query.setParameter("phrase", searchPhrase);
		return query.getResultList();
	}

	@NotNull
	@Override
	public List<Task> findByDescription(@NotNull final String userId, @NotNull final String description) {
		@NotNull final TypedQuery<Task> query = entityManager.createQuery(
				"SELECT t FROM Task t WHERE t.user.id = :userId AND t.description LIKE :description ORDER BY t.description",
				Task.class
		);
		query.setParameter("userId", userId);
		query.setParameter("description", description);
		return query.getResultList();
	}

	@Override
	public void remove(@NotNull String id) throws InterruptedOperationException {
		@Nullable final Task task = findOne(id);
		if (task == null) throw new InterruptedOperationException();
		entityManager.remove(task);
	}

	@Override
	public void removeById(@NotNull final String userId, @NotNull final String taskId) throws InterruptedOperationException {
		@Nullable final Task task = findOneById(userId, taskId);
		if (task == null) throw new InterruptedOperationException();
		entityManager.remove(task);
	}

	@Override
	public void removeAll(@NotNull final String userId) {
		for (@NotNull final Task task : findAllByUserId(userId))
			entityManager.remove(task);
	}

}
