package ru.ahmetahunov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.entity.User;
import ru.ahmetahunov.tm.exception.InterruptedOperationException;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    public void persist(@NotNull User user);

    public void merge(@NotNull User user);

    @Nullable
    public User findOne(@NotNull String id);

    @Nullable
    public User findUser(@NotNull String login);

    @NotNull
    public List<User> findAll();

    public void remove(@NotNull String id) throws InterruptedOperationException;

    public void removeAll();

}
