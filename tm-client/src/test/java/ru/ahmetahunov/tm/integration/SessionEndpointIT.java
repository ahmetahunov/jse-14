package ru.ahmetahunov.tm.integration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.ahmetahunov.tm.api.endpoint.*;
import ru.ahmetahunov.tm.endpoint.SessionEndpointImplService;
import ru.ahmetahunov.tm.endpoint.UserEndpointImplService;

public class SessionEndpointIT {

	@NotNull
	private final SessionEndpoint sessionEndpoint = new SessionEndpointImplService().getSessionEndpointImplPort();

	@NotNull
	private final UserEndpoint userEndpoint = new UserEndpointImplService().getUserEndpointImplPort();

	@Test
	public void createSessionExceptionTest() {
		Assert.assertThrows(
				AccessForbiddenException_Exception.class,
				() -> sessionEndpoint.createSession(null, null)
		);
		Assert.assertThrows(
				AccessForbiddenException_Exception.class,
				() -> sessionEndpoint.createSession(null, "")
		);
		Assert.assertThrows(
				AccessForbiddenException_Exception.class,
				() -> sessionEndpoint.createSession("", null)
		);
		Assert.assertThrows(
				AccessForbiddenException_Exception.class,
				() -> sessionEndpoint.createSession("", "")
		);
		Assert.assertThrows(
				AccessForbiddenException_Exception.class,
				() -> sessionEndpoint.createSession("unknown", "unknown")
		);
	}

	@Test
	public void createSessionOkTest() throws Exception {
		@NotNull final String token = sessionEndpoint.createSession("admin", "admin");
		Assert.assertNotNull(token);
		@Nullable final UserDTO user = userEndpoint.findUser(token);
		Assert.assertNotNull(user);
		sessionEndpoint.removeSession(token);
	}

	@Test
	public void removeSessionTest() throws Exception {
		@NotNull final String token = sessionEndpoint.createSession("admin", "admin");
		Assert.assertNotNull(token);
		@Nullable final UserDTO user = userEndpoint.findUser(token);
		Assert.assertNotNull(user);
		sessionEndpoint.removeSession(token);
		Assert.assertThrows(
				AccessForbiddenException_Exception.class,
				() -> userEndpoint.findUser(token)
		);
		Assert.assertThrows(
				AccessForbiddenException_Exception.class,
				() -> userEndpoint.findUser(null)
		);
	}

}
