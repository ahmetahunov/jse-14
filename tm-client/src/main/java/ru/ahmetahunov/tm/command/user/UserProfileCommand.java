package ru.ahmetahunov.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ahmetahunov.tm.api.endpoint.UserDTO;
import ru.ahmetahunov.tm.api.service.ITerminalService;
import ru.ahmetahunov.tm.command.AbstractCommand;
import ru.ahmetahunov.tm.util.InfoUtil;

@NoArgsConstructor
public final class UserProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-profile";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show user's profile information.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @Nullable final String session = serviceLocator.getStateService().getSession();
        @Nullable final UserDTO user = serviceLocator.getUserEndpoint().findUser(session);
        terminalService.writeMessage("[USER PROFILE]");
        terminalService.writeMessage(InfoUtil.getUserInfo(user));
    }

}
