
package ru.ahmetahunov.tm.api.endpoint;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.3.6
 * 2020-05-26T01:37:00.071+03:00
 * Generated source version: 3.3.6
 */

@WebFault(name = "InterruptedOperationException", targetNamespace = "http://endpoint.api.tm.ahmetahunov.ru/")
public class InterruptedOperationException_Exception extends Exception {

    private ru.ahmetahunov.tm.api.endpoint.InterruptedOperationException interruptedOperationException;

    public InterruptedOperationException_Exception() {
        super();
    }

    public InterruptedOperationException_Exception(String message) {
        super(message);
    }

    public InterruptedOperationException_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public InterruptedOperationException_Exception(String message, ru.ahmetahunov.tm.api.endpoint.InterruptedOperationException interruptedOperationException) {
        super(message);
        this.interruptedOperationException = interruptedOperationException;
    }

    public InterruptedOperationException_Exception(String message, ru.ahmetahunov.tm.api.endpoint.InterruptedOperationException interruptedOperationException, java.lang.Throwable cause) {
        super(message, cause);
        this.interruptedOperationException = interruptedOperationException;
    }

    public ru.ahmetahunov.tm.api.endpoint.InterruptedOperationException getFaultInfo() {
        return this.interruptedOperationException;
    }
}
